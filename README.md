<h1>kameguns.v7</h1>
かめ銃(オリジナル弾道プラグイン)<br>
このプラグインはkameplayerv4を前提としています、導入する際はそちらも同時に導入してください<br>
* [ダウンロード kameguns7](https://gitlab.com/kameme1208/kameguns7/issues)

* [ダウンロード kameplayer](https://gitlab.com/kameme1208/kameplayer)


<h2>銃の使い方</h2>

<h3>追加コマンド</h3>
|コマンド|短縮系|パーミッション|説明|
|----|----|----|----|
|/kameguns|/kg|kameguns.user|かめ銃の設定GUIを開く|
|/kameguns reload|/kg reload|kameguns.admin|Configの再読み込み|

<h3>パーミッション</h3>


|パーミッション|初期値|説明|
|----|----|----|----|
|kameguns.user|true|かめ銃の設定を開く権限|
|kameguns.admin|op|管理者コマンドの権限|
|kameguns.interact.shoot|true|発射の権限|
|kameguns.interact.reload|true|リロードの権限|
|kameguns.interact.zoom|true|スコープモードの権限|
|kameguns.param.nolimit|op|リミッターを超えた性能の使用権限|
|kameguns.param.gun.\<パラメーター>※1|true|本体性能のデフォルトからの変更権限|
|kameguns.param.option.\<パラメーター>※1|true|拡張性能のデフォルトからの変更権限|
|kameguns.param.ammo.\<パラメーター>※1|true|銃弾性能のデフォルトからの変更権限|
|kameguns.param.spell.<スペル名>※2|true|スペルの使用権限|
※1: パラメーター    「sound, speed, damnage, gravity, resist, expand, particle, color, recoil, back, delay, changedelay, gunshot, multishot, random, sneak, scope, offsetscope]<br>
※2: スペル名    「expand, fire, lightning, lightningeffect, potion, takaitakai」

変更権限が無い場合はその値のデフォルトの値が適応されて発射されます。<br>
リミッターでの制限を受けた場合、範囲外の数値はその範囲内に一番近い数字に書き換えられます。<br>

<h3>GUIの使い方</h3>

<h2>銃の作り方(サーバー管理者向け)</h2>

<h4>プラグイン本体に銃のGiveコマンドジェネレーターが同梱されています、NBTを設定するよりもこちらを使った方が楽だと思います。</h4>

------
プラグイン本体をダブルクリックすれば開きます。
使い方 ....
 * jarファイルをダブルクリックで起動(起動しない方はそもそもの設定おかしいので何とかしてください)
 * チェックボックスにポチポチしたり数値弄ったりしていい感じのを作る。
 * "クリップボードにコピー"のボタンを押す。
 * マイクラのコマンドブロックに張り付けて実行。
 * すっごーい！なにこれなにこれー！！


![キャプチャ](https://gitlab.com/kameme1208/picture/uploads/cd74e7dc86b77c4aba1051e84d0b347e/%E3%82%AD%E3%83%A3%E3%83%97%E3%83%81%E3%83%A3.PNG)


<h1>銃アイテムのNBT値</h1>

* 最小構成<br>

```json
{"KameGuns":{"Type":"simplegun"}}
```

<h1>かめ銃アドオン:スペルの追加方法</h1>

* 開発者向け ***KameGuns7 beta2以降の機能***

  
  かめ銃にはスペルを追加することでスペルを記述した弾が着弾したときそのプログラムが実行されます。
  
  プラグインの作成方法はある程度調べてくださいプラグイン有効時の```onEnable()```に```Spell.register(String, class)```をするだけです。
  
-----

* おてほん 着弾した場所に雷を落とす
　

　1 Spellクラスをextendsしたクラスを作る

```java
public class Lightning extends Spell {
	@Override
 	public void call(Entity shooter, CollisionPosition pos, String[] args) {
  		Location loc = pos.getPosition();               //着弾位置の取得
   		loc.getWorld().strikeLightning(loc);            //着弾位置に雷を落とす
   	}
}
```

  2 プラグイン起動時にSpellクラスに名前とクラスを登録する
  
```java
public class ExampleSpell extends JavaPlugin {
    @Override
    public void onEnable() {
       Spell.register("らいとにんぐ", new Lightning());
    }
}
```

これで銃の弾のスペル設定に「らいとにんぐ」と記載すると着弾地点に雷が発生します