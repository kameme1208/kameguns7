package kame.kameguns.baseutils;

import java.util.List;
import java.util.Map;

import org.bukkit.inventory.ItemStack;

import kame.api.baseapi.NBTUtils;

public class ParamGun implements Param {
	public double speed;
	public double damage;
	public double gravity;
	public double resist;
	public float expand;

	public String particle;
	public String sound;
	public String color;

	public short zoomtype;
	public boolean zoom;
	public boolean measure;

	public float pitch;

	public float recoil;
	public double back;
	public int delay;
	public int gunshot;
	public int multishot;
	public int changedelay;
	public double random;
	public double sneak;
	public double scope;
	public double offsetscope;

	public ItemStack obj;

	public int reload;
	public ItemStack item;
	public List<Object> attachmentTypes;
	public String spell;

	private Map<String, Object> param;

	public ParamGun(Map<String, Object> param) {
		this.param = param;
		this.sound		= NBTUtils.getString(param.get("Sound")	, "default");

		Map<String, Object> state	= NBTUtils.getMap(param.get("State"));
		this.speed		= NBTUtils.getNumber(state.get("speed")	, 1).doubleValue();
		this.damage		= NBTUtils.getNumber(state.get("damage")	, 5).doubleValue();
		this.gravity	= NBTUtils.getNumber(state.get("gravity")	, 9.8).doubleValue();
		this.resist		= NBTUtils.getNumber(state.get("resist")	, 1).doubleValue();
		this.expand		= NBTUtils.getNumber(state.get("expand")	, 0).floatValue();
		this.particle	= NBTUtils.getString(state.get("particle")	, "reddust");
		this.color		= NBTUtils.getString(state.get("color")	, "black");

		Map<String, Object> scope	= NBTUtils.getMap(param.get("Scope"));
		this.zoomtype	= NBTUtils.getNumber(scope.get("Type")		, -1).shortValue();
		this.zoom		= NBTUtils.getNumber(scope.get("Zoom")		, 0).intValue() != 0;
		this.measure	= NBTUtils.getNumber(scope.get("Measure")	, 0).intValue() != 0;

		Map<String, Object> shoot	= NBTUtils.getMap(param.get("Shoot"));
		this.recoil			= NBTUtils.getNumber(shoot.get("recoil")		, 0).floatValue();
		this.changedelay	= NBTUtils.getNumber(shoot.get("changedelay")	, 0).intValue();
		this.back			= NBTUtils.getNumber(shoot.get("back")			, 0).doubleValue();
		this.delay			= NBTUtils.getNumber(shoot.get("delay")		, 10).intValue();
		this.gunshot		= NBTUtils.getNumber(shoot.get("gunshot")		, 1).intValue();
		this.multishot		= NBTUtils.getNumber(shoot.get("multishot")	, 1).intValue();
		this.random			= NBTUtils.getNumber(shoot.get("random")		, 0).doubleValue();
		this.sneak			= NBTUtils.getNumber(shoot.get("sneak")		, 1).doubleValue();
		this.scope			= NBTUtils.getNumber(shoot.get("scope")		, 1).doubleValue();
		this.offsetscope	= NBTUtils.getNumber(shoot.get("offsetscope")	, -0.2).doubleValue();

		Map<String, Object> reload	= NBTUtils.getMap(param.get("Reload"));
		this.reload		= NBTUtils.getNumber(reload.get("reload")	, 10).intValue();
		this.item		= NBTUtils.createItemStack(NBTUtils.getMap(reload.get("Item")));
		if(reload.containsKey("Type") && !reload.containsKey("SingleReload"))this.item = null;
		if(reload.containsKey("Ammo"))ParamAmmo.modifyState(this, NBTUtils.getMap(reload.get("Ammo")));

		Map<String, Object> option	= NBTUtils.getMap(param.get("Custom"));
		this.pitch		= NBTUtils.getNumber(option.get("ModifyPitch")	, 0.0f).floatValue();
		Map<String, Object> customitem	 = NBTUtils.getMap(option.get("CustomItem"));
		List<Object> list = NBTUtils.getList(customitem.get("Up"));
		if(list.removeIf(x -> modifyItem(NBTUtils.getMap(x))))customitem.put("Up", list);
		list = NBTUtils.getList(customitem.get("Down"));
		if(list.removeIf(x -> modifyItem(NBTUtils.getMap(x))))customitem.put("Down", list);
		list = NBTUtils.getList(customitem.get("Left"));
		if(list.removeIf(x -> modifyItem(NBTUtils.getMap(x))))customitem.put("Left", list);
		list = NBTUtils.getList(customitem.get("Right"));
		if(list.removeIf(x -> modifyItem(NBTUtils.getMap(x))))customitem.put("Right", list);
		option.put("CustomItem", customitem);
		this.param.put("Custom", option);
		this.attachmentTypes = NBTUtils.getList(param.get("AttachmentTypes"));
	}

	public Map<String, Object> getMap() {
		return param;
	}

	public String breaksound;

	private boolean modifyItem(Map<String, Object> map) {
		map = NBTUtils.getMap(NBTUtils.getMap(map.get("nbt")).get("KameGunsParts"));
		ParamOption.modifyState(this, map);
		if(map.containsKey("Fail")) {
			if(NBTUtils.getNumber(map.get("Fail"), 0).doubleValue() > Math.random()) {
				breaksound = NBTUtils.getString(map.get("BreakSound"), "default");
				return true;
			}
		}
		return false;
	}
}
