package kame.kameguns.guns.projectile;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import org.bukkit.entity.Entity;

import kame.kameguns.guns.spells.Explode;
import kame.kameguns.guns.spells.Fire;
import kame.kameguns.guns.spells.Lightning;
import kame.kameguns.guns.spells.LightningEffect;
import kame.kameguns.guns.spells.Potion;
import kame.kameguns.guns.spells.TakaiTakai;

public abstract class Spell {
	private static Map<String, Spell> spells = new HashMap<>();

	protected Spell() {}

	public static void reset() {
		register("fire", new Fire());
		register("lightning", new Lightning());
		register("lightningeffect", new LightningEffect());
		register("potion", new Potion());
		register("explode", new Explode());
		register("高い他界", new TakaiTakai());
	}

	static void run(Entity shooter, Collision pos, @Nullable String spell) throws Exception {
		if(spell == null)return;
		String[] args = spell.split(":");
		if(spells.containsKey(args[0]))spells.get(args[0]).call(shooter, pos, args);
	}

	public static void register(String resisterName, Spell clazz) {
		if(clazz.getClass().equals(Spell.class))throw new IllegalArgumentException("Do not resister \"Spell.class\". It will loop!!");
		spells.put(resisterName, clazz);
	}

	public static void unregister(String resisterName) {
		spells.remove(resisterName);
	}

	public abstract void call(Entity shooter, Collision pos, String[] args);
}
