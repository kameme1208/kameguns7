package kame.kameguns.guns.projectile;

import javax.annotation.Nonnull;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;

public class Collision {
	private Entity entity;
	private Block block;
	private BlockFace face;
	private Location loc;

	public Collision(@Nonnull Location loc, @Nonnull BlockFace face) {
		this.loc = loc;
		this.face = face;
	}

	public Collision(@Nonnull Location loc, @Nonnull BlockFace face, Block block) {
		this.loc = loc;
		this.face = face;
		this.block = block;
	}

	public Collision(@Nonnull Location loc, @Nonnull BlockFace face, Entity entity) {
		this.loc = loc;
		this.face = face;
		this.entity = entity;
	}

	Collision(Location loc, BlockFace face, Block block, Entity entity) {
		this.loc = loc;
		this.face = face;
		this.block = block;
		this.entity = entity;
	}

	public Location getLocation() {
		return loc.clone();
	}

	public BlockFace getFace() {
		return face;
	}

	public Entity getEntity() {
		return entity;
	}

	public Block getBlock() {
		return block;
	}

	public boolean isBlock() {
		return block != null;
	}

	public boolean isEntity() {
		return entity != null;
	}

	public boolean isBox() {
		return entity == null && block == null;
	}

}
