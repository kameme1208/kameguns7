package kame.kameguns.guns.projectile;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import kame.api.baseapi.CollisionPosition;

public abstract class Ball {
	private final Location startloc;
	protected Vector motion;
	protected Location loc;
	protected Location hitloc;
	protected final LivingEntity shooter;

	protected Ball(LivingEntity shooter, Location loc, Vector motion) {
		this.shooter = shooter;
		this.startloc = loc.clone();
		this.loc = loc;
		this.motion = motion;
	}

	public abstract boolean checkCollision();

	public abstract void nextVector();

	public void hit() {}

	public Location getStartLocation() {
		return startloc.clone();
	}

	protected void call(Entity shooter, CollisionPosition pos, String spell) {
		try {
			Spell.run(shooter, new Collision(pos.getLocation(), pos.getDirection(), pos.getBlock(), pos.getEntity()), spell);
		} catch (Exception e) {
			shooter.sendMessage("§cスペルの処理に失敗しました 原因:" + e.getMessage());
			e.printStackTrace();
		}
	}
}
