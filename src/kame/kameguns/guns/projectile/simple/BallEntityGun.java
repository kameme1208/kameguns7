package kame.kameguns.guns.projectile.simple;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

import kame.api.baseapi.Collider;
import kame.api.baseapi.CollisionPosition;
import kame.kameguns.Main;
import kame.kameguns.baseutils.GunObject;
import kame.kameguns.baseutils.ParamGun;
import kame.kameguns.listener.InteractListener;

public class BallEntityGun extends BallSimpleGun {
	protected static final MaterialData REDSTONE = new ItemStack(Material.REDSTONE_BLOCK).getData();
	private int count;
	protected GunObject obj;

	public BallEntityGun(Player shooter, Location loc, Vector motion, ParamGun param) {
		super(shooter, loc, motion, param);
		Location dir = oldloc.clone();
		if(!InteractListener.hasScope(shooter)) {
			dir.setPitch(0);
			dir.setYaw(dir.getYaw() + 90);
			obj = new GunObject(oldloc.clone().subtract(0, 0.2, 0).add(dir.getDirection().multiply(0.2)), param.obj);
		}else {
			obj = new GunObject(oldloc, param.obj);
		}
	}

	@Override
	protected boolean checkCollision(boolean flag) {
		CollisionPosition pos = Collider.rayTraceWorld(oldloc, loc, noCollision, flag, false, false);
		if(pos.isHit()) {
			hitloc = pos.getLocation();
			obj.move(hitloc);
			if(pos.isBlock()) {
				return blockHit(pos);
			}else {
				return entityHit(pos);
			}
		}else {
			if(count++ == 400)return true;
			if(hitloc != null)hitloc = null;
			obj.move(loc);
			return false;
		}
	}

	protected boolean result(CollisionPosition pos, LivingEntity entity) {
		call(shooter, pos, spell);
		return true;
	}

	@Override
	public void hit() {
		if(hitloc == null)return;
		explode();
		if(shooter.getWorld().equals(hitloc.getWorld()) && shooter.getLocation().distance(hitloc) > 16)
			shooter.spawnParticle(Particle.FIREWORKS_SPARK, hitloc, 16, 0, 0, 0, 0.1);
		hitloc.getWorld().playSound(hitloc, Sound.ENTITY_ARROW_HIT, 1, 1);
		Bukkit.getScheduler().runTaskLaterAsynchronously(Main.getInstance(), obj::hit, 10);
	}

}
