package kame.kameguns.guns.projectile.simple;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import kame.api.baseapi.Collider;
import kame.api.baseapi.CollisionPosition;
import kame.kameguns.baseutils.Color;
import kame.kameguns.baseutils.GunParticle;
import kame.kameguns.baseutils.ParamGun;
import kame.kameguns.guns.projectile.Ball;

public class BallSimpleGun extends Ball {
	protected static final MaterialData REDSTONE = new ItemStack(Material.REDSTONE_BLOCK).getData();
	private int count;
	protected double speed = 1;
	protected double damage = 0;
	protected double gravity = 0.049;
	protected double resist = 0.001;
	protected float expand = 0;
	protected String particle = "reddust";
	protected float dX = 0;
	protected float dY = 0;
	protected float dZ = 0;
	protected Location oldloc;
	protected Player shooter;
	protected Set<Player> players = new HashSet<>();
	protected Collection<Entity> noCollision = new HashSet<>();
	protected String spell;
	protected CollisionPosition lasthit;

	public BallSimpleGun(Player shooter, Location loc, Vector motion, ParamGun param) {
		super(shooter, loc, motion);
		this.shooter = shooter;
		oldloc = loc.clone();
		noCollision.add(shooter);
		players.addAll(loc.getWorld().getPlayers());
		players.remove(shooter);

		particle= param.particle;
		speed	= param.speed;
		damage	= param.damage;
		expand	= param.expand;
		gravity	= param.gravity / 200d;
		resist	= param.resist / 3000d;
		spell	= param.spell;

		Color c = Color.getColor(param.color);
		dX = c.r();
		dY = c.g();
		dZ = c.b();
		loc.add(motion.multiply(16).multiply(speed));
		if(param.multishot > 6)loc.add(motion.clone().multiply(Math.random()));
		addPath(shooter.getVehicle());
		addPath(shooter.getVehicle());
		loc.getWorld().getPlayers().forEach(this::addTeam);
	}

	private void addPath(Entity path) {
		if(path != null)noCollision.add(path);
	}

	private void addTeam(Player player) {
		for(Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) if(!team.allowFriendlyFire()){
			if(team.hasEntry(shooter.getName()) && team.hasEntry(player.getName())) {
				addPath(player);
				return;
			}
		}
	}

	@Override
	public boolean checkCollision() {
		return checkCollision(true);
	}

	protected boolean checkCollision(boolean flag) {
		CollisionPosition pos = Collider.rayTraceWorld(oldloc, loc, noCollision, flag, false, false);
		if(pos.isHit()) {
			hitloc = pos.getLocation();
			return pos.isBlock() ?  blockHit(pos) : entityHit(pos);
		}else {
			if(count++ == 400)return true;
			if(hitloc != null)hitloc = null;
			GunParticle.send(shooter, players, particle, oldloc, loc, 0.02f, dX, dY, dZ);
			return false;
		}
	}

	protected boolean blockHit(CollisionPosition pos) {
		Block block = pos.getBlock();
		Material mate = block.getType();
		if(mate == Material.STATIONARY_WATER) {
			GunParticle.send(shooter, players, "bubble", loc, hitloc, 0.1f, 0, 0, 0);
			hitloc = null;
			double length = motion.length();
			motion.multiply(1 / (1 + resist * length * 2000));
			return checkCollision(false);
		}
		if(shooter.getWorld().equals(hitloc.getWorld()) && shooter.getLocation().distance(hitloc) > 16) {
			shooter.spawnParticle(Particle.FIREWORKS_SPARK, hitloc, 4, 0, 0, 0, 0.1);
		}
		GunParticle.send(shooter, players, particle, oldloc, hitloc, 0.02f, dX, dY, dZ);
		return result(pos, block);
	}

	protected boolean result(CollisionPosition pos, Block block) {
		pos.getLocation().getWorld().spawnParticle(Particle.BLOCK_DUST, pos.getLocation(), 16, 0, 0, 0, 0.1, block.getState().getData());
		call(shooter, pos, spell);
		return true;
	}

	protected boolean entityHit(CollisionPosition pos) {
		LivingEntity entity = (LivingEntity) pos.getEntity();
		if(pos.getLocation().getY() > entity.getEyeLocation().getY()) {
			damage *= 2;
			shooter.playSound(shooter.getLocation(), Sound.BLOCK_ANVIL_LAND, 0.5f, 2);
		}else {

			shooter.playSound(shooter.getLocation(), Sound.ENTITY_ARROW_HIT, 0.5f, 2);
		}
		entity.damage(motion.length() / 16f * damage, shooter);
		entity.setNoDamageTicks(0);
		pos.getLocation().getWorld().spawnParticle(Particle.BLOCK_CRACK, pos.getLocation(), 16, 0, 0, 0, 0.1, REDSTONE);
		return result(pos, entity);
	}

	protected boolean result(CollisionPosition pos, LivingEntity entity) {
		GunParticle.send(shooter, players, particle, oldloc, hitloc, 0.02f, dX, dY, dZ);
		call(shooter, pos, spell);
		return true;
	}

	protected void explode() {
		if(expand != 0)
			hitloc.getWorld().createExplosion(hitloc.getX(), hitloc.getY(), hitloc.getZ(), Math.abs(expand), false, false);
	}
	@Override
	public void hit() {
		if(hitloc == null)return;
		explode();
		if(shooter.getWorld().equals(hitloc.getWorld()) && shooter.getLocation().distance(hitloc) > 16)
			shooter.spawnParticle(Particle.FIREWORKS_SPARK, hitloc, 16, 0, 0, 0, 0.1);
		hitloc.getWorld().playSound(hitloc, Sound.ENTITY_ARROW_HIT, 1, 1);
	}

	@Override
	public void nextVector() {
		double length = motion.length();
		motion.multiply(1 / (1 + resist * length * length));
		motion.setY(motion.getY() - gravity);
		oldloc.setX(loc.getX());
		oldloc.setY(loc.getY());
		oldloc.setZ(loc.getZ());
		loc.add(motion);
	}
}
