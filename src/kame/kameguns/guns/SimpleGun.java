package kame.kameguns.guns;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import kame.api.KameAPI;
import kame.api.baseapi.NBTUtils;
import kame.kameguns.Config.SoundType;
import kame.kameguns.Main;
import kame.kameguns.SoundConfig;
import kame.kameguns.baseutils.ParamGun;
import kame.kameguns.guns.projectile.Ball;
import kame.kameguns.guns.projectile.Ballistics;
import kame.kameguns.guns.projectile.simple.BallEntityGun;
import kame.kameguns.guns.projectile.simple.BallEntityMultiHitHopGun;
import kame.kameguns.guns.projectile.simple.BallMultiHitHopGun;
import kame.kameguns.guns.projectile.simple.BallSimpleGun;
import kame.kameguns.listener.InteractListener;

public class SimpleGun extends KameGuns {

	private static Random rnd = new Random();

	private static Pattern lores = Pattern.compile("([^\\[]+)\\[([0-9]+)/([0-9]+)\\]");

	public SimpleGun(Player shooter, ItemStack item) {
		super(shooter, item);
	}

	@Override
	public void shoot(Map<String, Object> nbt) {
		if(super.containsDelay(shooter))return;
		ParamGun param = new ParamGun(nbt);
		if(shooter instanceof Player && ((Player) shooter).isSneaking()) {
			param.recoil *= param.sneak;
			param.back   *= param.sneak;
			param.random *= param.sneak;
		}
		if(param.breaksound != null) {
			NBTUtils.setNBTTag(item, "KameGuns", param.getMap());
			SoundConfig.playWorldSound(shooter.getEyeLocation(), SoundType.OPTION_BREAK, param.breaksound);
			if(shooter instanceof Player) {
				KameAPI.sendActionbar((Player) shooter, "§eオプションパーツ§cが破損しました");
			}
		}
		Ammo ammo = new Ammo();
		if(itemCheck(param, ammo)) {
			shootBall(param, ammo);
			super.putDelay(shooter, param.delay, param.sound);
		}
	}

	private boolean itemCheck(ParamGun param, Ammo ammo) {
		ItemMeta im = item.getItemMeta();
		List<String> lore = im.hasLore() ? im.getLore() : new ArrayList<>();
		for(int i = 0; i < lore.size(); i++) {
			Matcher m = lores.matcher(lore.get(i));
			if(m.matches()) {
				ammo.amount = Integer.parseInt(m.group(2));
				ammo.maxamount = Integer.parseInt(m.group(3));
				if(ammo.amount == 0) {
					if(shooter instanceof Player) {
						((Player) shooter).playSound(shooter.getEyeLocation(), Sound.BLOCK_DISPENSER_FAIL, 1, 2);
						setscore((Player)shooter, ammo.amount, ammo.maxamount);
					}
					return false;
				}
				ammo.amount -= param.multishot;
				if(ammo.amount < 0) {
					param.multishot += ammo.amount;
					ammo.amount = 0;
				}
				lore.set(i, m.group(1) + "[" + ammo.amount + "/" + m.group(3) + "]");
				im.setLore(lore);
				item.setItemMeta(im);
				shooter.getEquipment().setItemInMainHand(item);
				return true;
			}
		}
		return true;
	}

	private void shootBall(ParamGun param, Ammo ammo) {
		BukkitTask task = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), () ->  {
			if(ammo.maxamount != -1 && shooter instanceof Player)
				setscore((Player)shooter, ammo.amount + param.multishot - ammo.multishot, ammo.maxamount);
		}, 0, 0);
		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), () -> {
			multishot(param, ammo);
			task.cancel();
		});
	}

	private void multishot(ParamGun param, Ammo ammo) {
		while(ammo.multishot++ < param.multishot) {
			gunshot(param, param.gunshot);
			Location playerloc = shooter.getLocation();
			if(param.recoil != 0) {
				playerloc.setPitch(playerloc.getPitch() - (rnd.nextFloat() - 0.4f) * param.recoil * 5);
				playerloc.setYaw(playerloc.getYaw() - (rnd.nextFloat() - 0.5f) * param.recoil);
				shooter.teleport(playerloc);
			}
			if(param.back != 0) {
				Vector move = shooter.getVelocity();
				Vector vec = playerloc.getDirection().multiply(param.back);
				shooter.setVelocity(move.subtract(vec));
			}
			SoundConfig.playWorldSound(shooter.getLocation(), SoundType.SHOOT, param.sound);
			Location pop = playerloc.clone();
			pop.setYaw(pop.getYaw() + 45);
			shooter.getWorld().spawnParticle(Particle.LAVA, shooter.getEyeLocation().add(pop.getDirection()), 1, 0, 0, 0, 0.1);
			try {
				Thread.sleep(400 / (param.multishot + 1));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void gunshot(ParamGun param, int gunshot) {
		for(int loop = 0; loop < gunshot; loop++) {
			Location loc = KameAPI.getPlayerEye((Player) shooter);
			loc.setPitch(loc.getPitch() - param.pitch);
			if(InteractListener.hasScope(shooter)) {
				loc.add(0, param.offsetscope, 0);
				param.recoil *= param.scope;
				param.back   *= param.scope;
				param.random *= param.scope;
			}
			Vector motion = loc.getDirection().add(new Vector(rnd.nextGaussian(), rnd.nextGaussian(), rnd.nextGaussian()).multiply(param.random));
			motion.add(shooter.getVelocity().multiply(new Vector(1, 0, 1)));
			Ball ball = param.obj == null ? hoppingBall(param.getMap()) ?
					new BallMultiHitHopGun((Player) shooter, loc.clone(), motion, param) : new BallSimpleGun((Player) shooter, loc.clone(), motion, param) :
						hoppingBall(param.getMap()) ?
					new BallEntityMultiHitHopGun((Player) shooter, loc.clone(), motion, param) : new BallEntityGun((Player) shooter, loc.clone(), motion, param);
			Ballistics.shootBall(ball);
		}

	}

	private boolean hoppingBall(Map<String, Object> param) {
		Map<String, Object> hithop = NBTUtils.getMap(param.get("State"));
		return hithop.containsKey("multihit") || hithop.containsKey("hitresist") || hithop.containsKey("hopping") || hithop.containsKey("hopresist");
	}

	private class Ammo {
		private int amount = -1;
		private int multishot = 0;
		private int maxamount = -1;
		private Map<String, Object> ammonbt;
	}

	@Override
	public void reload(Map<String, Object> nbt) {
		if(super.containsDelay(shooter))return;
		ParamGun param = new ParamGun(nbt);
		Ammo ammo = new Ammo();

		ItemStack reloaditem = param.item;
		if(reloaditem == null) {
			List<Object> type = NBTUtils.getList(NBTUtils.getMap(nbt.get("Reload")).get("Type"));
			Inventory inv = ((Player) shooter).getInventory();
			for(ItemStack items : inv)if(items != null && NBTUtils.getNBTKeys(items).contains("KameGunsAmmo")) {
				Map<String, Object> reloadnbt = NBTUtils.getMap(NBTUtils.getNBTTag(items, "KameGunsAmmo"));
				List<Object> list =NBTUtils.getList(reloadnbt.get("Type"));
				if(list.removeAll(type)) {
					reloadnbt.remove("Type");
					ammo.amount = NBTUtils.getNumber(reloadnbt.remove("Amount"), -1).intValue();
					ammo.ammonbt = reloadnbt;
					reloaditem = items.clone();
					reloaditem.setAmount(1);
					break;
				}
			}
			if(reloaditem == null) {
				((Player)shooter).playSound(shooter.getLocation(), Sound.BLOCK_LAVA_POP, 2, 2);
				return;
			}
		}else {
			int amount = reloaditem.getAmount();
			if(!reloaditem.getType().equals(Material.AIR) && shooter instanceof Player) {
				int invamount = 0;
				Inventory inv = ((Player) shooter).getInventory();
				for(ItemStack items : inv)if(items != null) {
					reloaditem.setAmount(items.getAmount());
					if(items.equals(reloaditem))invamount += items.getAmount();
				}
				if(invamount < amount) {
					((Player)shooter).playSound(shooter.getLocation(), Sound.BLOCK_LAVA_POP, 2, 2);
					return;
				}
				reloaditem.setAmount(amount);
			}
		}
		ItemMeta im = super.item.getItemMeta();
		List<String> lore = im.hasLore() ? im.getLore() : new ArrayList<>();
		for(int i = 0; i <= lore.size(); i++) {
			if(i == lore.size())return;
			Matcher m = lores.matcher(lore.get(i));
			if(!m.matches())continue;
			ammo.maxamount = Integer.parseInt(m.group(3));
			if(ammo.amount == -1)ammo.amount = ammo.maxamount;
			if(m.group(2).equals(m.group(3)))return;
			lore.set(i, m.group(1) + "[" + ammo.amount + "/" + m.group(3) + "]");
			im.setLore(lore);
			((HumanEntity) shooter).getInventory().removeItem(reloaditem);
			break;
		}
		super.item.setItemMeta(im);
		if(nbt.containsKey("Reload")) {
			Map<String, Object> reload = NBTUtils.getMap(nbt.get("Reload"));
			if(reload.containsKey("Ammo")) {
				reload.remove("Ammo");
				nbt.put("Reload", reload);
				NBTUtils.setNBTTag(item, "KameGuns", nbt);
			}
		}
		if(ammo.ammonbt != null) {
			Map<String, Object> reload = NBTUtils.getMap(nbt.get("Reload"));
			reload.put("Ammo", ammo.ammonbt);
			nbt.put("Reload", reload);
			NBTUtils.setNBTTag(item, "KameGuns", nbt);
		}
		shooter.getEquipment().setItemInMainHand(item);

		if(ammo.maxamount != -1 && shooter instanceof Player) {
			Bukkit.getScheduler().runTaskLater(Main.getInstance(), () ->
			setscore((Player)shooter, ammo.amount , ammo.maxamount), param.reload * 4);
		}
		super.putDelay(shooter, param.reload * 5, "");
		SoundConfig.playReloadSound(shooter, param.reload, param.sound);
	}

}
