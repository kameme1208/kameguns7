package kame.kameguns.guns.spells;

import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import kame.kameguns.guns.projectile.Collision;
import kame.kameguns.guns.projectile.Spell;

public class TakaiTakai extends Spell {

	@Override
	public void call(Entity shooter, Collision pos, String[] args) {
		if(pos.isEntity()) {
			pos.getEntity().setVelocity(new Vector(0, args.length > 0 ? parse(args[0]) : 1, 0));
		}
	}

	private float parse(String line) {
		try {
			return Float.parseFloat(line);
		}catch(NumberFormatException e) {
			return 1;
		}
	}
}
