package kame.kameguns.guns.spells;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;

import kame.kameguns.guns.projectile.Collision;
import kame.kameguns.guns.projectile.Spell;

public class Fire extends Spell {

	@Override
	public void call(Entity shooter, Collision pos, String[] args) {
		if(pos.isEntity()){
			pos.getEntity().setFireTicks(args.length > 0 ? parse(args[0]) : 100);
		}
		Location loc = pos.getLocation();
		loc.getWorld().spawnParticle(Particle.FLAME, loc, 10, 0, 0, 0, 0.01);
	}

	private int parse(String line) {
		try {
			return Integer.parseInt(line);
		}catch(NumberFormatException e) {
			return 100;
		}
	}

}
