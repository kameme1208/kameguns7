package kame.kameguns.guns.spells;

import org.bukkit.entity.Entity;

import kame.kameguns.guns.projectile.Collision;
import kame.kameguns.guns.projectile.Spell;

public class LightningEffect extends Spell {

	@Override
	public void call(Entity shooter, Collision pos, String[] args) {
		pos.getLocation().getWorld().strikeLightningEffect(pos.getLocation());
	}

}
