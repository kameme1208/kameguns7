package kame.kameguns.guns.spells;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffectType;

import kame.kameguns.guns.projectile.Collision;
import kame.kameguns.guns.projectile.Spell;

public class Potion extends Spell {

	@Override
	public void call(Entity shooter, Collision pos, String[] args) {
		if(pos.isEntity()){
			Entity entity = pos.getEntity();
			if(entity instanceof LivingEntity) {
				PotionEffectType effect = null;
				int pow = 0, time = 0;
				try {
					if(args.length > 0)effect = PotionEffectType.getByName(args[1]);
					if(args.length > 1)time = Integer.parseInt(args[1]);
					if(args.length > 2)pow = Integer.parseInt(args[2]);
				}catch(Exception e) {}
				if(effect == null)effect = PotionEffectType.POISON;
				((LivingEntity) entity).addPotionEffect(effect.createEffect(time, pow));
			}
		}
	}

}
