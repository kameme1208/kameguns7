package kame.kameguns.guns.spells;

import org.bukkit.entity.Entity;

import kame.kameguns.guns.projectile.Collision;
import kame.kameguns.guns.projectile.Spell;

public class Lightning extends Spell {

	@Override
	public void call(Entity shooter, Collision pos, String[] args) {
		pos.getLocation().getWorld().strikeLightning(pos.getLocation());
	}

}
