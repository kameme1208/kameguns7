package kame.kameguns.guns.spells;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

import kame.kameguns.guns.projectile.Collision;
import kame.kameguns.guns.projectile.Spell;

public class Explode extends Spell {

	@Override
	public void call(Entity shooter, Collision pos, String[] args) {
		float power = 0;
		boolean fire = false;
		boolean destroy = false;
		if(args.length > 1) power = Float.parseFloat(args[1]);
		if(args.length > 2) fire = args[2].equals("1");
		if(args.length > 3) destroy = args[3].equals("1");
		Location loc = pos.getLocation();
		loc.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), power, fire, destroy);
	}

}
