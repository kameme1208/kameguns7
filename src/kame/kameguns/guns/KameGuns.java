package kame.kameguns.guns;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;

import kame.api.baseapi.NBTUtils;
import kame.kameguns.Config.SoundType;
import kame.kameguns.Main;
import kame.kameguns.SoundConfig;

public abstract class KameGuns {

	private static Map<String, Class<? extends KameGuns>> guns = new HashMap<>();
	protected LivingEntity shooter;
	protected ItemStack item;

	public KameGuns(Player shooter, ItemStack item) {
		this.shooter = shooter;
		this.item = item;
	}

	public abstract void shoot(Map<String, Object> nbt);
	public abstract void reload(Map<String, Object> nbt);
	public void change(Map<String, Object> nbt) {
		Map<String, Object> shoot = NBTUtils.getMap(nbt.get("Shoot"));
		int changedelay = NBTUtils.getNumber(shoot.get("changedelay"), 10).intValue();
		Integer time = Timer.timer.get(shooter);
		if(time == null || time < changedelay) {
			Timer.timer.put(shooter, changedelay);
			Timer.sound.put(shooter, NBTUtils.getString(nbt.get("Sound"), "default"));
		}
	}



	public static void register(String resigterName, Class<? extends KameGuns> clazz) {
		if(clazz.equals(KameGuns.class))throw new IllegalArgumentException("Do not resister \"KameGuns.class\". It will loop!!");
		guns.put(resigterName, clazz);
	}

	public static void unregister(String resigterName) {
		guns.remove(resigterName);
	}



	public static void shoot(Player shooter, Map<String, Object> nbt, ItemStack item) {
		try {
			Class<? extends KameGuns> gun = guns.get(nbt.get("Type"));
			if(gun == null)throw new IllegalArgumentException("Type: 登録されてないGunType:" + nbt.get("Type"));
			Constructor<? extends KameGuns> con = gun.getDeclaredConstructor(Player.class, ItemStack.class);
			con.setAccessible(true);
			con.newInstance(shooter, item).shoot(nbt);
		} catch (Throwable e) {
			shooter.sendMessage(ChatColor.RED + "[KameGuns] 銃の発射に失敗しました");
			shooter.sendMessage(ChatColor.RED + "原因" + e.getMessage());
			StringJoiner joiner = new StringJoiner("\n\t");
			joiner.add("[KameGuns] 銃の発射に失敗しました");
			for(StackTraceElement el : e.getStackTrace()) {
				joiner.add(el.toString());
			}
			Bukkit.getLogger().severe(joiner.toString());
		}
	}

	public static void reload(Player shooter, Map<String, Object> nbt, ItemStack item) {
		try {
			Class<? extends KameGuns> gun = guns.get(nbt.get("Type"));
			if(gun == null)throw new IllegalArgumentException("Type: 登録されてないGunType" + guns);
			Constructor<? extends KameGuns> con = gun.getDeclaredConstructor(Player.class, ItemStack.class);
			con.setAccessible(true);
			con.newInstance(shooter, item).reload(nbt);
		} catch (Throwable e) {
			shooter.sendMessage(ChatColor.RED + "[KameGuns] 銃のリロードに失敗しました");
			shooter.sendMessage(ChatColor.RED + "原因" + e.getMessage());
			StringJoiner joiner = new StringJoiner("\n\t");
			joiner.add("[KameGuns] 銃のリロードに失敗しました");
			for(StackTraceElement el : e.getStackTrace()) {
				joiner.add(el.toString());
			}
			Bukkit.getLogger().severe(joiner.toString());
		}
	}

	public static void change(Player shooter, Map<String, Object> nbt, ItemStack item) {
		try {
			Class<? extends KameGuns> gun = guns.get(nbt.get("Type"));
			if(gun == null)throw new IllegalArgumentException("Type: 登録されてないGunType" + guns);
			Constructor<? extends KameGuns> con = gun.getDeclaredConstructor(Player.class, ItemStack.class);
			con.setAccessible(true);
			con.newInstance(shooter, item).change(nbt);
		} catch (Throwable e) {
			shooter.sendMessage(ChatColor.RED + "[KameGuns] 銃の持ち替えに失敗しました");
			shooter.sendMessage(ChatColor.RED + "原因" + e.getMessage());
			StringJoiner joiner = new StringJoiner("\n\t");
			joiner.add("[KameGuns] 銃のリロードに失敗しました");
			for(StackTraceElement el : e.getStackTrace()) {
				joiner.add(el.toString());
			}
			Bukkit.getLogger().severe(joiner.toString());
		}
	}

	private static class Timer extends BukkitRunnable{

		private static Map<Entity, Integer> timer = new HashMap<>();
		private static Map<Entity, String> sound = new HashMap<>();

		static {
			new Timer().runTaskTimer(Main.getInstance(), 0, 1);
		}

		private Timer(){}

		@Override
		public void run() {
			timer.entrySet().removeIf(this::subTimer);
		}

		private boolean subTimer(Entry<Entity, Integer> entry) {
			if(entry.setValue(entry.getValue()-1) <= 0) {
				if(entry.getKey() instanceof Player) {
					SoundConfig.playSound(entry.getKey(), SoundType.RECOIL, sound.get(entry.getKey()));
				}
				return true;
			}else {
				return false;
			}
		}
	}

	public void putDelay(Entity player, int time, String sound) {
		Timer.timer.put(player, time);
		Timer.sound.put(player, sound);
	}

	public boolean containsDelay(Entity shooter) {
		return Timer.timer.containsKey(shooter);
	}

	protected void setscore(Player player, int amount,int maxamount) {
		player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		for(Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			Team buffer = player.getScoreboard().registerNewTeam(team.getName());
			buffer.setAllowFriendlyFire(team.allowFriendlyFire());
			buffer.setCanSeeFriendlyInvisibles(team.canSeeFriendlyInvisibles());
			buffer.setDisplayName(team.getDisplayName());
			buffer.setPrefix(team.getPrefix());
			buffer.setSuffix(team.getSuffix());
			for(Option op : Option.values()) {
				buffer.setOption(op, team.getOption(op));
			}
			team.getEntries().forEach(buffer::addEntry);
		}
		Objective obj = player.getScoreboard().registerNewObjective("残り弾数  §c"+ (amount), "");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		obj.getScore("最大弾数").setScore(maxamount);
		Display.timer.put(player.getUniqueId(), 10);
	}

	private static class Display extends BukkitRunnable{

		private static Map<UUID, Integer> timer = new HashMap<>();

		static {
			new Display().runTaskTimer(Main.getInstance(), 0, 20);
		}

		private Display(){}

		@Override
		public void run() {
			timer.entrySet().removeIf(this::isLeave);
		}

		private boolean isLeave(Entry<UUID, Integer> entry) {
			if(entry.setValue(entry.getValue()-1) <= 0) {
				Player player = Bukkit.getPlayer(entry.getKey());
				if(player != null && player.isOnline()) {
					player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
				}
				return true;
			}
			return false;
		}
	}
}
